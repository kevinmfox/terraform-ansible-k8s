- [Overview](#overview)
- [Assumptions](#assumptions)
- [Terraform - Ansible Deployment](#terraform---ansible-deployment)
- [Ansible - Initial Setup](#ansible---initial-setup)
- [PowerShell - Create Inputs for Kubernetes Deployment](#powershell---create-inputs-for-kubernetes-deployment)
- [Terraform - Kubernetes Deployment](#terraform---kubernetes-deployment)
- [PowerShell - Create Ansible Inventory (hosts) File](#powershell---create-ansible-inventory-hosts-file)
- [Ansible/Kubernetes - All Server Initialization](#ansiblekubernetes---all-server-initialization)
- [Ansible/Kubernetes - Control Plane Initialization](#ansiblekubernetes---control-plane-initialization)
- [Ansible/Kubernetes - Node Initialization](#ansiblekubernetes---node-initialization)
- [Quick Check](#quick-check)
- [Test Web Application Deployment](#test-web-application-deployment)
- [Teardown Help](#teardown-help)
- [AWS Gotchas](#aws-gotchas)
    - [AWS IAM Roles](#aws-iam-roles)
    - [AWS Hostnames](#aws-hostnames)
    - [AWS Tagging](#aws-tagging)
    - [AWS Cloud Provider](#aws-cloud-provider)

<a name="overview"></a>
# Overview
Initially this started out as me just wanting to learn a little more about Kubernetes after learning some stuff about [Docker](https://bitbucket.org/kevinmfox/docker-app/src/master/). 

But then I hit a bit of a snag. My lab environment is in AWS, and though, in retrospect, the setup actually wasn't all that complex, I struggled for quite a while to get a proper Kubernetes deployment on AWS EC2 (not EKS). This boiled down to most tutorials not covering how to integrate with a cloud provider (AWS in my case), and the fact that I struggled to find 'clear' documentation on how to do so with AWS. If you're looking for those [AWS Gotchas](#aws-gotchas), jump down to that section at the bottom.

Since I was continuously making changes to my instances, they couldn't be trusted. So, of course I started using Terraform and Ansible to help quickly re-deploy my test environment.

The below covers:

* Using Terraform to setup the 'managemnet' environment (where Ansible will live)
* Initial Ansible setup
* Using Terraform to deploy the Kubernetes cluster
* Using Ansible to initialize all hosts
* Using Ansible to setup the Control Plane node
* Using Ansible to setup the nodes
* Deploying a test application

On top of the automation, I've included commentary on some specifics I think are important.

For reference, I was running Terraform and the few PowerShell scripts from my local Windows 10 machine. All servers (Ansible & Kubernetes) were Ubuntu 20.04.

<a name="assumptions"></a>
# Assumptions

To run the Terraform scrips you'll need to have already setup an AWS API Key (and, obviously, an AWS account). 

I have mine set on my system so it doesn't appear in the script, but the [documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs) does allow for the access_key and secret_key to be embedded directly into the provisioning script (though, I wouldn't recommend that).

I'm also assuming you have Terraform installed and have _some_ familiarity with it (though, not required). If you're scared of Terraform, don't be ;). The install is dead simple, and a well-written script will run itself :).

<a name="terraform---ansible-deployment"></a>
# Terraform - Ansible Deployment

Head on into the Terraform/AWS-Mgmt folder.

There are some variables that will need to be modified within the main.tf file to account for a different environment.

Run:
```
terraform init
terraform plan
terraform apply -auto-approve
```

This script will deploy the initial VPC, Networking elements, and Server to be used for Ansible.

<a name="ansible---initial-setup"></a>
# Ansible - Initial Setup

Grab the Public IP of your Ansible server, and SSH into it.

Grab the ```ansible-install.sh``` script from Ansible/Scripts and put it on the server. Make it executable (```chmod +x ansible-install.sh```).

Run: ```./ansible-install.sh```

This will (obvioulsy) install Ansible on the system, and will most likely reboot the server. It will also set the default ansible user to 'ubuntu', and will set the config to skip host key checking.

Copy the SSH key you'll be using for Ansible communications to the Ansible system. For my lab, I just used my AWS "Lab01.pem" key so that I didn't need to update my 'authorized_keys' file on each system. This certainly isn't a best practice, but it works for this lab.

Copy the relevant SSH key to (e.g.) ```/home/ubuntu/.ssh/Lab01.pem``` and give it the appropraite permissoins (e.g.) ```chmod 600 /home/ubuntu/.ssh/Lab01.pem```

<a name="powershell---create-inputs-for-kubernetes-deployment"></a>
# PowerShell - Create Inputs for Kubernetes Deployment

Head on over to Terraform/Scripts.

Run: ```createTFK8sInputs.ps1``` (PowerShell script).

This will take the outputs from the previous Terraform Ansible run, and put them into a ```aws-mgmt.auto.tfvars``` file in the Terraform/AWS-Kubernetes folder.

The Kubernetes Terraform deployment will use these variables.

<a name="terraform---kubernetes-deployment"></a>
# Terraform - Kubernetes Deployment

Head on into the Terraform/AWS-Kubernetes folder.
https://kubernetes.github.io/cloud-provider-aws/prerequisites/
There are some variables that will need to be modified within the main.tf file to account for a different environment.
The script is also expecting some values from the Ansible Terraform deployment. Those should have been created in the last phase.

Run:
```
terraform init
terraform plan
terraform apply -auto-approve
```

This script will deploy the Kubernetes Control Plane server as well as several (3 by default) Kubernetes nodes.

<a name=#powershell---create-ansible-inventory-hosts-file""></a>
# PowerShell - Create Ansible Inventory (hosts) File

Head on over to Terraform/Scripts.

Run: ```createAnsibleInventory.ps1``` (PowerShell script).

This will take the outputs from the previous Terraform Kubernetes run, and generate an Ansible 'hosts' file.

The generated file will be located at Ansible/Assets/hosts.

Copy this hosts file and place it on the Ansible server at: ```/etc/ansible/hosts```

<a name="ansiblekubernetes---all-server-initialization"></a>
# Ansible/Kubernetes - All Server Initialization

Copy the Ansible/Playbooks folder to the Ansible server.

On the Ansible server, navigate into the Ansible/Playbooks folder.

Update the ```01-kube-init.yaml``` with the appropriate SSH key (```ansible_ssh_private_key_file: ~/.ssh/Lab01.pem```)

Run: ```ansible-playbook 01-kube-init.yaml```

After this all nodes will have Kubernetes (kubeadm, kubectl, kubelet) and containerd installed.

<a name="ansiblekubernetes---control-plane-initialization"></a>
# Ansible/Kubernetes - Control Plane Initialization

On the Ansible server, navigate into the Ansible/Playbooks folder.

Update the ```02-kube-controlplane.yaml``` with the appropriate SSH key (```ansible_ssh_private_key_file: ~/.ssh/Lab01.pem```)

Run: ```ansible-playbook 02-kube-controlplane.yaml```

After this the Control Plane node will be ready to go.

<a name="ansiblekubernetes---node-initialization"></a>
# Ansible/Kubernetes - Node Initialization

On the Anible server, navigate into the Ansible/Playbooks folder.

Update the ```03-kube-node.yaml``` with the appropriate SSH key (```ansible_ssh_private_key_file: ~/.ssh/Lab01.pem```)

Run: ```ansible-playbook 03-kube-node.yaml -e "api_server=[api_server_ip]```

Where ```api_server_ip``` is the IP address of the Control Plane node.

After this the nodes will have joined the cluster.

<a name="quick-check"></a>
# Quick Check

At this point, you should be able to log into your Control Plane node, and run the following command: ```kubectl get nodes```

![](/assets/image01.jpg)

<a name="test-web-application-deployment"></a>
# Test Web Application Deployment

Grab the hello-world.yaml file from the Kubernetes folder and put that on the Control Plane node.

Run the following command to deploy the application: ```kubectl apply -f hello-world.yaml```

The run ```kubectl get all``` to see the status of the cluster. Should look something like this:

![](/assets/image02.jpg)

If all went to play, you should see the AWS DNS endpoint registered as the External IP.

It will take a minute or two for the instances to pass their health checks, but if you navigate to that website, you should see:

![](/assets/image03.jpg)

<a name="teardown-help"></a>
# Teardown Help

Delete the entire deployment: ```kubectl delete -f hello-world.yaml```

Or, delete the service and deployment:
```
kubectl delete service hello-world-service
kubectl delete deployment hello-world
```

Within Terraform/AWS-Kubernetes: ```terraform destroy -auto-approve```

Within Terraform/AWS-Mgmt: ```terraform destroy -auto-approve```

<a name="aws-gotchas"></a>
# AWS Gotchas

After a lot of reading, troubleshooting, failing, redeploying, crying...getting this to work in AWS boiled down to a few required elements.

<a name="aws-iam-roles"></a>
### AWS IAM Roles

The IAM Roles that need to be defined for both the control plane and regular nodes can be found here:
[https://kubernetes.github.io/cloud-provider-aws/prerequisites/](https://kubernetes.github.io/cloud-provider-aws/prerequisites/)

I'll put them here for convienence.

***Control Plane***
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:DescribeAutoScalingGroups",
        "autoscaling:DescribeLaunchConfigurations",
        "autoscaling:DescribeTags",
        "ec2:DescribeInstances",
        "ec2:DescribeRegions",
        "ec2:DescribeRouteTables",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeSubnets",
        "ec2:DescribeVolumes",
        "ec2:CreateSecurityGroup",
        "ec2:CreateTags",
        "ec2:CreateVolume",
        "ec2:ModifyInstanceAttribute",
        "ec2:ModifyVolume",
        "ec2:AttachVolume",
        "ec2:AuthorizeSecurityGroupIngress",
        "ec2:CreateRoute",
        "ec2:DeleteRoute",
        "ec2:DeleteSecurityGroup",
        "ec2:DeleteVolume",
        "ec2:DetachVolume",
        "ec2:RevokeSecurityGroupIngress",
        "ec2:DescribeVpcs",
        "elasticloadbalancing:AddTags",
        "elasticloadbalancing:AttachLoadBalancerToSubnets",
        "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer",
        "elasticloadbalancing:CreateLoadBalancer",
        "elasticloadbalancing:CreateLoadBalancerPolicy",
        "elasticloadbalancing:CreateLoadBalancerListeners",
        "elasticloadbalancing:ConfigureHealthCheck",
        "elasticloadbalancing:DeleteLoadBalancer",
        "elasticloadbalancing:DeleteLoadBalancerListeners",
        "elasticloadbalancing:DescribeLoadBalancers",
        "elasticloadbalancing:DescribeLoadBalancerAttributes",
        "elasticloadbalancing:DetachLoadBalancerFromSubnets",
        "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
        "elasticloadbalancing:ModifyLoadBalancerAttributes",
        "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
        "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer",
        "elasticloadbalancing:AddTags",
        "elasticloadbalancing:CreateListener",
        "elasticloadbalancing:CreateTargetGroup",
        "elasticloadbalancing:DeleteListener",
        "elasticloadbalancing:DeleteTargetGroup",
        "elasticloadbalancing:DescribeListeners",
        "elasticloadbalancing:DescribeLoadBalancerPolicies",
        "elasticloadbalancing:DescribeTargetGroups",
        "elasticloadbalancing:DescribeTargetHealth",
        "elasticloadbalancing:ModifyListener",
        "elasticloadbalancing:ModifyTargetGroup",
        "elasticloadbalancing:RegisterTargets",
        "elasticloadbalancing:DeregisterTargets",
        "elasticloadbalancing:SetLoadBalancerPoliciesOfListener",
        "iam:CreateServiceLinkedRole",
        "kms:DescribeKey"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
```

***Node***
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeInstances",
        "ec2:DescribeRegions",
        "ecr:GetAuthorizationToken",
        "ecr:BatchCheckLayerAvailability",
        "ecr:GetDownloadUrlForLayer",
        "ecr:GetRepositoryPolicy",
        "ecr:DescribeRepositories",
        "ecr:ListImages",
        "ecr:BatchGetImage"
      ],
      "Resource": "*"
    }
  ]
}
```

These policies allow the nodes to interact with AWS services.

This is all _baked into_ the AWS-Kubernetes Terraform script.

<a name="aws-hostnames"></a>
### AWS Hostnames

The AWS Hostnames (i.e. ```hostname```) needs to match the Private IP DNS Name in the AWS Console. This can also be _pulled_ by running the following command on the EC2 instance directly: ```curl http://169.254.169.254/latest/meta-data/local-hostname```

This is set during the Ansible Kubernetes initial setup (i.e. 01-kube-init.yaml).

<a name="aws-tagging"></a>
### AWS Tagging

Any relevant AWS Subnets and Instances need to be tagged so that Kubernetes knows it can/should interact with those components.

The tag format is: ```kubernetes.io/cluster/[cluster_name]```

Where cluster_name can be anything you'd like.

This is all taken care of in the AWS-Kubernetes Terraform script.

<a name="awscloudprovider"></a>
### AWS Cloud Provider

Two commands can be used within Kubernetes to generate the default initialization scripts:
```
kubeadm config print init-defaults
kubeadm config print join-defaults
```

init-defaults is used to create the control plane initialization script; join-defaults is used for the nodes.

What's missing in both of these is the ```cloud-provider: aws``` line for the apiServer, controllerManager, and nodeRegistration sections.

The ```Ansible/Playbooks/Assets/cluster-configuration.yaml``` and ```Ansible/Playbooks/Assets/node-join-defaults.yaml``` files both have the aws lines built-in.