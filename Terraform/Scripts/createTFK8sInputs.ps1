# this script will take the outputs from the AWS-Mgmt Terraform script, and write them to a AWS-Kubernetes auto.tfvars file

# save the current location and move into the AWS-Mgmt folder
Write-Host "Saving location"
Push-Location -Path "..\AWS-Mgmt"

# get the terraform outputs as a json doc
Write-Host "Get Terraform output"
$json = ConvertFrom-Json ([String](terraform output --json))
Write-Host (terraform output --json)

# write the required variables into the Terraform\AWS-Kubernetes\aws-mgmt.auto.tfvars file
Write-Host "Generating aws-mgmt.auto.tfvars file"
Write-Host ("vpc_id: " + $json.vpc_id.value)
("vpc_id = """ + $json.vpc_id.value + """") | Out-File -FilePath "..\AWS-Kubernetes\aws-mgmt.auto.tfvars" -Encoding utf8
Write-Host ("route_table_id: " + $json.route_table_id.value)
("route_table_id = """ + $json.route_table_id.value + """") | Out-File -FilePath "..\AWS-Kubernetes\aws-mgmt.auto.tfvars" -Append -Encoding utf8
Write-Host ("mgmt_security_group_id: " + $json.mgmt_security_group_id.value)
("mgmt_security_group_id = """ + $json.mgmt_security_group_id.value + """") | Out-File -FilePath "..\AWS-Kubernetes\aws-mgmt.auto.tfvars" -Append -Encoding utf8

# jump back to the script folder
Write-Host "Jumping back to start location"
Pop-Location