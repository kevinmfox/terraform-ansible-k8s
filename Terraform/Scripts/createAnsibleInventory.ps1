# this script will generate the Ansible hosts (/etc/ansible/hosts) file
# the output will go into the Ansible/Assets/hosts file
# it uses the output from the Terraform Kubernetes deployment

# save the current location and move into the AWS-Kubernetes folder
Write-Host "Saving location"
Push-Location -Path "..\AWS-Kubernetes"

# get the terraform outputs as a json doc
Write-Host "Get Terraform output"
$json = ConvertFrom-Json ([String](terraform output --json))
Write-Host (terraform output --json)

# write the output to Ansible\Assets\hosts in the proper format
"" | Out-File -FilePath "..\..\Ansible\Assets\hosts" -Encoding utf8
("[k8s_control_plane]") | Out-File -FilePath "..\..\Ansible\Assets\hosts" -Append -Encoding utf8
Write-Host ("control_plane_ip: " + $json.control_plane_ip.value)
($json.control_plane_ip.value) | Out-File -FilePath "..\..\Ansible\Assets\hosts" -Append -Encoding utf8
"" | Out-File -FilePath "..\..\Ansible\Assets\hosts" -Append -Encoding utf8
("[k8s_nodes]") | Out-File -FilePath "..\..\Ansible\Assets\hosts" -Append -Encoding utf8
foreach ($nodeIp in $json.node_ips)
{
    Write-Host ("node_ip: " + $nodeIp.value)
    ($nodeIp.value) | Out-File -FilePath "..\..\Ansible\Assets\hosts" -Append -Encoding utf8
}
"" | Out-File -FilePath "..\..\Ansible\Assets\hosts" -Append -Encoding utf8
("[k8s_nodes:vars]") | Out-File -FilePath "..\..\Ansible\Assets\hosts" -Append -Encoding utf8
("api_server_ip=" + $json.control_plane_ip.value) | Out-File -FilePath "..\..\Ansible\Assets\hosts" -Append -Encoding utf8

# jump back to the script folder
Write-Host "Jumping back to start location"
Pop-Location