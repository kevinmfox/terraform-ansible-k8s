output "vpc_id" {
  description = "ID of the VPC"
  value = aws_vpc.main.id
}

output "route_table_id" {
  description = "ID of the Route Table"
  value = aws_route_table.main.id
}

output "mgmt_security_group_id" {
  description = "ID of the management Security Group"
  value = aws_security_group.mgmt.id
}
