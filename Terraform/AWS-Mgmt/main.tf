# this script will:
#  create a VPC
#  create a subnet
#  create a security group
#  create an internet gateway
#  create a routing table
#  associate the subnet with the routing table
#  launch a server into the subnet
#  attach an EIP to the server
# will output (outputs.tf):
#  mgmt_security_group_id
#  route_table_id
#  vpc_id
# These outputs are used by the Kubernetes Terraform script

# adjust the below variables for your setup/enviroment
locals {
    vpc_cidr              = "10.10.0.0/16"
    mgmt_subnet           = "10.10.0.0/24"
    aws_ami_id            = "ami-04505e74c0741db8d" # us-east-1 Ubuntu 20.04
    aws_instance_type     = "t2.small"
    aws_region            = "us-east-1"
    aws_availability_zone = "us-east-1a"
    aws_key_pair_name     = "Lab01"
    aws_profile           = "default"
}

# used to get my IP address so I have access to the deployed system
data "http" "my_ip" {
    url = "http://ipv4.icanhazip.com"
}

# use for the AWS provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# configure the AWS provider
# note: credentials are stored on the system under my 'default' profile
provider "aws" {
  region    = local.aws_region
  profile   = local.aws_profile
}

# create the VPC
resource "aws_vpc" "main" {
  cidr_block = local.vpc_cidr
  enable_dns_hostnames	= true
  tags = {
      Name = "main"
  }
}

# create the management subnet
resource "aws_subnet" "mgmt" {
  vpc_id     = aws_vpc.main.id
  availability_zone = local.aws_availability_zone
  cidr_block = local.mgmt_subnet

  tags = {
    Name = "mgmt"
  }
}

# create the security group
# allow everything from my house; internal traffic within the subnet; everything outbound
resource "aws_security_group" "mgmt" {
  name                = "Management"
  description         = "Management"
  vpc_id              = aws_vpc.main.id

  ingress {
    description       = "Everything from Home"
    from_port         = 0
    to_port           = 0
    protocol          = "-1"
    cidr_blocks       = ["${chomp(data.http.my_ip.body)}/32"]
  }

  ingress {
    description       = "Allow internal traffic"
    from_port         = 0
    to_port           = 0
    protocol          = "-1"
    self              = true
  }

  egress {
    from_port         = 0
    to_port           = 0
    protocol          = "-1"
    cidr_blocks       = ["0.0.0.0/0"]
    ipv6_cidr_blocks  = ["::/0"]
  }
}

# create the internet gateway
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main"
  }
}

# create the route table
# local will be added by default; everyting else goes out the internet gateway
resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = {
    Name = "main"
  }
}

# create the route table associations
resource "aws_route_table_association" "main" {
  subnet_id      = aws_subnet.mgmt.id
  route_table_id = aws_route_table.main.id
}

# launch the ansible server
resource "aws_instance" "ansible" {
  ami                         = local.aws_ami_id
  instance_type               = local.aws_instance_type
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.mgmt.id]
  subnet_id                   = aws_subnet.mgmt.id
  key_name                    = local.aws_key_pair_name
  tags = {
      Name = "ansible"
  }
}

# get an EIP for the ansible server
resource "aws_eip" "ansible" {
  instance = aws_instance.ansible.id
  vpc      = true
}