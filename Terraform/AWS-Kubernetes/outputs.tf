output "control_plane_ip" {
  description = "IP of the Kubernetes Control Plane node"
  value = aws_instance.controlPlane.private_ip
}

output "node_ips" {
  description = "IP of the Kubernetes nodes"
  value = aws_instance.nodes[*].private_ip
}