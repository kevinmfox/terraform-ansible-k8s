#! /bin/bash

# upgrade the system
sudo apt update
sudo apt full-upgrade -y
sudo apt update

# install ansible
sudo apt install software-properties-common -y
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible -y

#clean up
sudo apt autoremove -y

# set the default remote user to 'ubuntu' and skip host key checking
echo "[defaults]" | sudo tee -a /etc/ansible/ansible.cfg
echo "remote_user=ubuntu" | sudo tee -a /etc/ansible/ansible.cfg
echo "host_key_checking=False" | sudo tee -a /etc/ansible/ansible.cfg

#reboot if we need to
[ -f /var/run/reboot-required ] && sudo reboot now
